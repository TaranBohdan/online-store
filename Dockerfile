#
# Build
#
FROM maven:3.9.5 AS build
WORKDIR /app
COPY . .
RUN mvn clean package

#
# Package
#
FROM amazoncorretto:21
WORKDIR /app
COPY --from=build /app/target/*.jar /app/app.jar
COPY src/main/resources/application-prod.properties /app/application.properties
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "app.jar", "--spring.profiles.active=prod"]