package org.teamchallenge.onlinestore.auth;

import lombok.Builder;

@Builder
public record AuthenticationResponse (String token) { }