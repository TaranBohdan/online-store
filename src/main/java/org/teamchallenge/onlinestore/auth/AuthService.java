package org.teamchallenge.onlinestore.auth;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.teamchallenge.onlinestore.handler.exception.UserAlreadyRegisteredException;
import org.teamchallenge.onlinestore.jwt.JwtService;
import org.teamchallenge.onlinestore.user.Role;
import org.teamchallenge.onlinestore.user.User;
import org.teamchallenge.onlinestore.user.UserRepository;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public AuthenticationResponse signup(SignUpDTO signUpDTO) {

        if (userRepository.findByEmail(signUpDTO.email()).isPresent()) {
            throw new UserAlreadyRegisteredException("This email is already registered. Please use another.");
        }

        var validEmail = "[a-z0-9.]+@[a-z0-9.]+\\.[a-z]+";

        if (!signUpDTO.email().matches(validEmail)) {
            throw new BadCredentialsException("Your email is invalid. Please try to enter another.");
        }

        var user = User.builder()
                .firstName(signUpDTO.firstName())
                .lastName(signUpDTO.lastName())
                .email(signUpDTO.email())
                .password(passwordEncoder.encode(signUpDTO.password()))
                .createdAt(LocalDateTime.now())
                .role(Role.USER)
                .build();

        userRepository.save(user);

        var jwtToken = jwtService.generateToken(user);

        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }

    public AuthenticationResponse login(LoginDTO loginDTO) {

        var email = loginDTO.email();
        var password = loginDTO.password();

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(email, password)
            );
        } catch (BadCredentialsException e) {
            throw new BadCredentialsException("The email address or password is incorrect. Please retry...");
        }

        var user = userRepository.findByEmail(email).orElseThrow();

        var jwtToken = jwtService.generateToken(user);

        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }
}
