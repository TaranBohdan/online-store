package org.teamchallenge.onlinestore.auth;

public record SignUpDTO (
        String firstName,
        String lastName,
        String email,
        String password
) {

}
