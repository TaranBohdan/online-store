package org.teamchallenge.onlinestore.auth;

public record LoginDTO (
        String email,
        String password
) {

}
