package org.teamchallenge.onlinestore.user;

public enum Role {

    USER,
    ADMIN
}
